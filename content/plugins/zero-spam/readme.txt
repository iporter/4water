=== WordPress Zero Spam ===
Contributors: bmarshall511
Donate link: https://www.gittip.com/bmarshall511/
Tags: comments, spam, antispam, anti-spam, comment spam, spambot, spammer, spam free, spam blocker
Requires at least: 3.0.0
Tested up to: 3.9.1
Stable tag: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Zero Spam makes blocking spam comments a cinch. Install, activate and enjoy a spam-free site.

== Description ==

**Why should your users prove that they are humans by filling captchas? Lets bots prove their are not bots the <a href="http://www.benmarshall.me/wordpress-zero-spam-plugin/" target="_blank">WordPress Zero Spam plugin</a>.**

WordPress Zero Spam blocks spam in comments automatically without any additional config or setup. Just install, actiavte and enjoy a spam-free site. Initially built based on the work by <a href="http://davidwalsh.name/wordpress-comment-spam" target="_blank">David Walsh</a>.

Major features in WordPress Zero Spam include:

* **No captcha**, because spam is not users' problem
* **No moderation queues**, because spam is not administrators' problem
* Blocks spam comments with this use of JavaScript

**Languages:** English

<a href="http://www.benmarshall.me" target="_blank">Follow me on Twitter</a> to keep up with the latest updates.

== Installation ==

1. Upload the `zero-spam` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= I keep getting 'There was a problem processing your comment.' =

Be sure JavaScript is enabled and there are no JS errors.

== Screenshots ==

== Changelog ==

= 1.1.0 =
* Updated theme documentation.
* WordPress generator meta tag removed to help hide WordPress sites from spambots.

= 1.0.0 =
* Initial release.

== Credits ==
* Thanks to [David Walsh](http://davidwalsh.name) [@davidwalshblog](https://twitter.com/davidwalshblog) for the inspiration behind this plugin.
